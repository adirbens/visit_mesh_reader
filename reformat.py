from socket import gethostname

try:
    from visit import *
except:
    print("Unable to load visit function :(")
    exit(1)


ROUND_FLOAT = 5


class MeshReformater:

    def __init__(self, dim, db_path, np):

        self._domains = {}
        
        self._dim = dim
        self._db_path = db_path
        self._mesh_coords = []

        self._host_name = gethostname()
        self._args = ("-n", str(np))


    def init_visit_server(self):
        
        try:
            LaunchNowin()#Launch() # LaunchNowin()
            OpenComputeEngine(self._host_name, self._args)
            OpenDatabase(self._db_path)
            DeleteAllPlots()
        except:
            print("Unable to open compute engine || database :(")
            exit(1)


    def create_domain_coords(self, mesh_name):

        try:
            AddPlot("Mesh", mesh_name)
            DrawPlots()
        except:
            print("Unable to load mesh '{}' :(".format(mesh_name))
            exit(1)

        self._add_domains_dims()
        self._add_domains_coords_list()
        self._add_domains_coords()
        self._add_domains_levels()
        self._round_coords()
        

        DeleteAllPlots()

    
    def _add_domains_dims(self):
        
        try:
            Query("Grid Information")
            grid_info = GetQueryOutputObject()
        except:
            print("Unable to load grid information :(")
            exit(1)

        for domain in grid_info:

            self._domains[domain] = {}
            self._domains[domain]["dimensions"] = grid_info[domain]["dimensions"]

            x_size = self._domains[domain]["dimensions"][0]
            y_size = self._domains[domain]["dimensions"][1]
            z_size = self._domains[domain]["dimensions"][2]

            zones_num = (x_size - 1) * (y_size - 1)
            if self._dim == 3:
                zones_num *= (z_size - 1)
            
            self._domains[domain]["cells_num"] = zones_num

    
    def _add_domains_coords_list(self):

        for domain in self._domains:

            domain_nodes = self._domains[domain]["dimensions"][0] \
                           * self._domains[domain]["dimensions"][1] \
                           * self._domains[domain]["dimensions"][2]
            
            self._domains[domain]["coords_num"] = domain_nodes
            self._domains[domain]["coords"] = [-1] * domain_nodes
            
    
    def _add_domains_coords(self):

        for domain in self._domains:
            for node in range(self._domains[domain]["coords_num"]):
                
                try:
                    Query("Node Coords", node, int(domain))
                    coord = GetQueryOutputValue()
                    self._domains[domain]["coords"][node] = coord
                except:
                    print("Unable to load coordinates of domain {} node {}".format(domain, node))
                    exit(1)

    
    def _add_domains_levels(self):

        md = GetMetaData(self._db_path)
        domains = md.GetMeshes(0).blockNames

        for domain in domains:

            level_patch = domain.split(",")          # Assumes that patch name is "level<i>,patch<j>"
            level = level_patch[0][5:]
            patch = level_patch[1][5:]

            self._domains[patch]["level"] = level
            print("DOMAIN", domain, "LEVEL", self._domains[patch]["level"], "\n") ###

    
    def _round_coords(self):
        
        for domain in self._domains:
            for node in range(self._domains[domain]["coords_num"]):
                
                coord_round = tuple(map(lambda x: round(x, ROUND_FLOAT) or x, self._domains[domain]["coords"][node]))
                self._domains[domain]["coords"][node] = coord_round
            print("DOMAIN", domain, "COORDS", self._domains[domain]["coords"], "\n") ###

    
    def create_nodal_vars(self, var_list):

        for variable in var_list:

            try:
                AddPlot("Vector", variable)         # Assumes that nodal var is a vector
                DrawPlots()
            except:
                print("Unable to load variable '{}' :(".format(variable))
                exit(1)

            for domain in self._domains:

                self._domains[domain][variable] = [-1] * self._domains[domain]["coords_num"]

                for node in range(self._domains[domain]["coords_num"]):
                    self._domains[domain][variable][node] = PickByNode(node, int(domain))[variable]
                print("DOMAIN", domain, "VARIABLE", variable, self._domains[domain][variable], "\n") ###
            DeleteAllPlots()


    def create_zonal_vars(self, var_list):
        
        for variable in var_list:

            try:
                AddPlot("Pseudocolor", variable)         # Assumes that nodal var is a scalar
                DrawPlots()
            except:
                print("Unable to load variable '{}' :(".format(variable))
                exit(1)

            for domain in self._domains:
                
                self._domains[domain][variable] = [-1] * self._domains[domain]["cells_num"]

                for cell in range(self._domains[domain]["cells_num"]):
                    self._domains[domain][variable][cell] = PickByZone(cell, int(domain))[variable]
                print("DOMAIN", domain, "VARIABLE", variable, self._domains[domain][variable], "\n") ###
            DeleteAllPlots()

    
    def finalize_visit_server(self):

        DeleteAllPlots()
        ClearAllWindows()
        CloseComputeEngine(self._host_name)
        CloseDatabase(self._db_path)
        Close()


def main():    
    
    mesh_reformater = MeshReformater(2, "/home/adirbens/projects/CleverLeaf_ref/build/triple_point.6.visit/dumps.visit", 4)
    mesh_reformater.init_visit_server()
    mesh_reformater.create_domain_coords("amr_mesh")
    mesh_reformater.create_nodal_vars(["Velocity"])
    mesh_reformater.create_zonal_vars(["Density"])
    mesh_reformater.finalize_visit_server()
    

if __name__ == "__main__":
    main()